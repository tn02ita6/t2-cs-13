#include<stdio.h>
int factorial(int x)
{
    int f=1;
    while(x!=0)
    {
        f=f*x;
        x--;
    }
    return f;
}
void main()
{
    int n,r,ncr;
    printf("Enter N and R values: ");
    scanf("%d %d",&n,&r);
    ncr=factorial(n)/(factorial(r)*factorial(n-r));
    printf("The NCR of %d and %d is %d",n,r,ncr);
}