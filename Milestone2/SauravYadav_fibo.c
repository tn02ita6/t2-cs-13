#include<stdio.h>
void fibonacci(int n) 
{ 
    int a= 0, b= 1, i; 
  
    if (n < 1) 
        return; 
  
    for (i=1;i<=n;i++) 
    { 
        printf("%d ", b); 
        int c=a+b; 
        a = b; 
        b = c; 
    } 
} 

int main() 
{ 
    int n;
    printf("Enter the limit:");
    scanf("%d",&n);
    fibonacci(n); 
    return 0; 
} 