#include<stdio.h>
#include<stdlib.h>
int a[20];
int m,n,p,val,i,j,key,pos,temp;
void display();
void insert();
void del();
void search();
void sort();
int main()
{
        int choice;
        printf("\nEnter the size of the array elements:\t");
        scanf("%d",&n);
        printf("\nEnter the elements:\n");
        for(i=0;i<n;i++)
        {
                scanf("%d",&a[i]);
        }
        do{
                printf("1.Insert\n2.Delete\n3.Search\n4.Sort\n5.Display\n6.Exit");
                printf("\nEnter your choice:\t");
                scanf("%d",&choice);
                switch(choice)
                {
                        case 1:
                                        insert();
                                        break;
                        case 2:
                                        del();
                                        break;
                        case 3:
                                        search();
                                        break;
                        case 4:
                                        sort();
                                        break;
                        case 5:
                                        display();
                                        break;
                        case 6:
                                        exit(0);
                                        break;
                        default:
                                        printf("\nEnter a valid choice:\n");
                                        break;
                }
        }while(choice!=6);
return 0;
}

void insert()
{         
    printf("\nEnter the position for the new element:\t");         
    scanf("%d",&pos);         
    printf("\nEnter the element to be inserted :\t");         
    scanf("%d",&val);  
    for(i=0;i<n;i++){
        if(val==a[i]){
            printf("Element already added.please try again.");
        }
    }
    for(i=n-1;i>=pos;i--)
        {
                a[i+1]=a[i];
        }
        a[pos]=val;
        n=n+1;
        printf("Element added successfully");
}


void del()
{
        printf("\nEnter the position of the element to be deleted:\t");
        scanf("%d",&pos);
        val=a[pos];
        for(i=pos;i<n-1;i++)
        {
                a[i]=a[i+1];
        }
        n=n-1;
        printf("\nThe deleted element is =%d",val);
}


void search()
{
        printf("\nEnter the element to be searched:\t");
        scanf("%d",&key);
        for(i=0;i<n;i++)
        {
                if(a[i]==key)
                {
                        printf("\nThe element is present at position %d",i);
                        break;
                }
        }
        if(i==n)
        {
                printf("\nThe search is unsuccessful");
        }
}

void sort()   
{
    int sort_choice;
    printf("Enter 1 for ascending sort, 2 for descending sort:");
    scanf("%d",&sort_choice);
    if(sort_choice==1){
        for(i=0;i<n-1;i++)
        {
                for(j=0;j<n-1-i;j++)                 {                         if(a[j]>a[j+1])
                        {
                                temp=a[j];
                                a[j]=a[j+1];
                                a[j+1]=temp;
                        }
                }
        }
        printf("\nAfter sorting the array elements in ascending order are:\n");
        display();
    }
    else if(sort_choice==2){
        
        for(i=0;i<n-1;i++)
        {
                for(j=0;j<n-1-i;j++)                 {                         if(a[j]<a[j+1])
                        {
                                temp=a[j];
                                a[j]=a[j+1];
                                a[j+1]=temp;
                        }
                }
        }
        printf("\nAfter sorting the array elements in ascending order are:\n");
        display();
        
        
    }
    else{
        printf("select appropriate number");
    }
}

void display() 
{
        int i;
        printf("\nThe array elements are:\n");
        for(i=0;i<n;i++){
                 printf("%d\t",a[i]);         
         }
 } 